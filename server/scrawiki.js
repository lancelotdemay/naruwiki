'use strict';

const express = require('express')
const fs = require('fs');
const app = express()
const fetch = require('node-fetch');

async function getData(type) {
  let query = type === 'movies' ? `PREFIX dc: <http://example.org/narutoMoviesV2>
                  SELECT ?id ?title_en ?title_jap ?date ?img {
                  ?id dc:title_en ?title_en.
                  ?id dc:title_jap ?title_jap.
                  ?id dc:date ?date.
                  ?id dc:img ?img.}` : 
                  `PREFIX dc: <http://example.org/narutoChronologyV2>
                  SELECT ?id ?title_en ?title_jap ?date {
                  ?id dc:title_en ?title_en.
                  ?id dc:title_jap ?title_jap.
                  ?id dc:date ?date.}`;
  let urlencoded = new URLSearchParams()
  urlencoded.append('query', query);
  const data = await fetch('https://sandbox.bordercloud.com/sparql', {
      method: 'POST',
      headers: {
        'Authorization': 'Basic ' + Buffer.from('ESGI-WEB-2020:ESGI-WEB-2020-heUq9f').toString('base64'),
        'Content-type': 'application/x-www-form-urlencoded'
      },
      body: urlencoded
    }).then(response => response.json())
      // .then(json => console.log(json))
      .catch(err => console.log(err))
  
  var res = data.results.bindings;
  var parsedRes = [];
  for (const obj of res) {
    let tmpInfos = {
      id: parseUriToId(obj.id.value),
      titre_eng: obj.title_en.value,
      titre_jp: obj.title_jap.value,
      date_jp: obj.date.value
    }
    if (type === 'movies') {
      tmpInfos['img'] = obj.img.value;
    }
    parsedRes.push(tmpInfos);
  }
  return parsedRes;
}

function parseUriToId(uri) {
  return parseInt(uri.split('/')[4], 16)
}

app.get('/movies', async function (req, res) {
  // let rawdata = fs.readFileSync('narutoMovies.json');
  // let movies = JSON.parse(rawdata);
  const data = await getData('movies');
  
  res.set('Content-Type', 'application/json; charset=UTF-8');
  res.set('Access-Control-Allow-Origin', '*');
  return res.send(data);
})

app.get('/episodes', async function (req, res) {
  const data = await getData('episodes');
  console.log(data);

  res.set('Content-Type', 'application/json; charset=UTF-8');
  res.set('Access-Control-Allow-Origin', '*');
  return res.send(data);
})

app.get('/insertEpisodes', function (req, res) {
  let rawdata = fs.readFileSync('./narutoShipudden.json');
  let episodes = JSON.parse(rawdata);

  for (let i = 0; i < 30; i++) {
    const tmpEpisode = episodes[i];
    let urlencoded = new URLSearchParams()
    urlencoded.append('update', `PREFIX dc:<http://example.org/narutoChronologyV2> 
                      INSERT DATA
                      {` + '<episode/' + tmpEpisode.numero + '> dc:title_en ' + '"' + tmpEpisode.titre_eng + '"' + ';' +
                        ' dc:title_jap ' + '"' + tmpEpisode.titre_jap + '"' + ' ;' +
                        ' dc:date ' + '"' + tmpEpisode.date_jp + '"' + '}');

    fetch('https://sandbox.bordercloud.com/sparql', {
      method: 'POST',
      headers: {
        'Authorization': 'Basic ' + Buffer.from('ESGI-WEB-2020:ESGI-WEB-2020-heUq9f').toString('base64'),
        'Content-type': 'application/x-www-form-urlencoded'
      },
      body: urlencoded
    })
      .then(response => response.json())
      .then(json => console.log(json))
      .catch(err => console.log(err))

  }
  return res.send('done');
})

app.get('/insertMovies', function (req, res) {
  let rawdata = fs.readFileSync('./narutoMovies.json');
  let movies = JSON.parse(rawdata);

  for (let i = 0; i < movies.length; i++) {
    const tmpMovies = movies[i];
    let urlencoded = new URLSearchParams()
    urlencoded.append('update', `PREFIX dc:<http://example.org/narutoMoviesV2> INSERT DATA {` + 
                    '<movies/' + tmpMovies.numero + '> dc:title_en ' + '"' + tmpMovies.titre_eng + '"' + ';' +
                    ' dc:title_jap ' + '"' + tmpMovies.titre_jap + '"' + ';' +
                    ' dc:date ' + '"' + tmpMovies.date_jp + '"' + ';'+
                    ' dc:img ' + '"' + tmpMovies.img + '"' + '}');
    console.log(urlencoded);
    
    fetch('https://sandbox.bordercloud.com/sparql', {
      method: 'POST',
      headers: {
        'Authorization': 'Basic ' + Buffer.from('ESGI-WEB-2020:ESGI-WEB-2020-heUq9f').toString('base64'),
        'Content-type': 'application/x-www-form-urlencoded'
      },
      body: urlencoded
    })
      .then(response => response.json())
      .then(json => console.log(json))
      .catch(err => console.log(err))

  }
  return res.send('done');
})


app.listen(3001, function () {
  console.log('Example app listening on port 3001!')
})